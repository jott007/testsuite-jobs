def reportDirectory = 'reports/*.xml'
def reportResult(directory) {
  junit directory
}

node {
  stage ("Clone") {
    git "https://gitlab.com/jott007/testsuite.git"
  }

  stage ("Test") {
    try {
      sh "mkdir -p reports"
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "gitlab", usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_PW']]) {
        sh "behave --junit --junit-directory reports"
      }
    } catch(err) {
      reportResult(reportDirectory)
      error "${err}"
    }
  }

  stage ("Report") {
    reportResult(reportDirectory)
  }  
}